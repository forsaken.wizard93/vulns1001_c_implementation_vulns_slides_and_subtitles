1
00:00:00,240 --> 00:00:04,880
stack buffer overflows what are those

2
00:00:02,800 --> 00:00:08,000
well it's when you have too much ACID

3
00:00:04,880 --> 00:00:10,559
data being copied from attacker's data

4
00:00:08,000 --> 00:00:12,799
into a vulnerable buffer on the stack so

5
00:00:10,559 --> 00:00:14,960
for instance if we had a memcpy and it

6
00:00:12,799 --> 00:00:16,880
had an attacker controlled length and an

7
00:00:14,960 --> 00:00:19,119
attacker-controlled data then an

8
00:00:16,880 --> 00:00:20,960
attacker could cause an overflow of this

9
00:00:19,119 --> 00:00:22,720
vulnerable buffer basically you know

10
00:00:20,960 --> 00:00:24,560
whatever the size of that buffer is if

11
00:00:22,720 --> 00:00:27,039
the attacker controls the size then they

12
00:00:24,560 --> 00:00:28,640
can just keep increasing the size until

13
00:00:27,039 --> 00:00:29,599
they ultimately exceed the bounds of the

14
00:00:28,640 --> 00:00:31,439
buffer

15
00:00:29,599 --> 00:00:33,280
so if this means something to you right

16
00:00:31,439 --> 00:00:34,960
now good great you know hopefully that

17
00:00:33,280 --> 00:00:36,960
was enough for you but if it wasn't

18
00:00:34,960 --> 00:00:38,480
let's go ahead and cover some background

19
00:00:36,960 --> 00:00:39,440
before we come back to the slide again

20
00:00:38,480 --> 00:00:41,840
later

21
00:00:39,440 --> 00:00:44,320
so first of all what is the stack well

22
00:00:41,840 --> 00:00:46,160
the stack can refer to a fundamental

23
00:00:44,320 --> 00:00:49,200
data structure type so if you think of

24
00:00:46,160 --> 00:00:50,879
things like linked lists or queues stack

25
00:00:49,200 --> 00:00:52,960
is another type of data structure

26
00:00:50,879 --> 00:00:55,360
specifically it's a data structure where

27
00:00:52,960 --> 00:00:57,600
it's called first in last out it's

28
00:00:55,360 --> 00:01:00,000
basically like a stack of plates so you

29
00:00:57,600 --> 00:01:01,920
put a plate onto the stack and you put

30
00:01:00,000 --> 00:01:03,600
another plate onto the stack and you

31
00:01:01,920 --> 00:01:04,799
just keep putting them on the top of the

32
00:01:03,600 --> 00:01:07,520
stack

33
00:01:04,799 --> 00:01:08,799
and so the first data that you put in is

34
00:01:07,520 --> 00:01:09,760
going to be at the bottom of the stack

35
00:01:08,799 --> 00:01:11,600
and you're not going to be able to get

36
00:01:09,760 --> 00:01:14,479
to that plate you can't pull it out from

37
00:01:11,600 --> 00:01:16,240
underneath you have to pop data off the

38
00:01:14,479 --> 00:01:18,479
top of the stack in order to get access

39
00:01:16,240 --> 00:01:20,080
to that earlier data so in this class

40
00:01:18,479 --> 00:01:22,000
when we refer to the stack we're not

41
00:01:20,080 --> 00:01:24,560
going to be referring to the data

42
00:01:22,000 --> 00:01:26,479
structure but rather a region of memory

43
00:01:24,560 --> 00:01:29,439
that behaves like that sort of data

44
00:01:26,479 --> 00:01:31,920
structure so in the memory area that's

45
00:01:29,439 --> 00:01:34,000
allocated for an executing program or a

46
00:01:31,920 --> 00:01:35,680
kernel or firmware or anything else if

47
00:01:34,000 --> 00:01:37,200
we imagine things with low addresses low

48
00:01:35,680 --> 00:01:39,119
and high address is high there might be

49
00:01:37,200 --> 00:01:40,479
some area that's reserved for the code

50
00:01:39,119 --> 00:01:41,680
that's going to execute

51
00:01:40,479 --> 00:01:44,159
and then there's an area that is

52
00:01:41,680 --> 00:01:47,280
reserved for the stack there's also an

53
00:01:44,159 --> 00:01:49,600
area reserved for the heap and so we say

54
00:01:47,280 --> 00:01:51,920
that the stack you know grows towards

55
00:01:49,600 --> 00:01:53,600
low addresses so basically the stack is

56
00:01:51,920 --> 00:01:54,399
effectively upside down in this sort of

57
00:01:53,600 --> 00:01:56,399
picture

58
00:01:54,399 --> 00:01:58,320
so basically the top of the stack here

59
00:01:56,399 --> 00:02:00,399
and it continues to grow towards lower

60
00:01:58,320 --> 00:02:02,079
addresses and the heap generally grows

61
00:02:00,399 --> 00:02:04,079
towards higher addresses

62
00:02:02,079 --> 00:02:06,719
so in the context of C programs the

63
00:02:04,079 --> 00:02:08,640
stack is used to store temporary data

64
00:02:06,719 --> 00:02:10,800
most importantly things like local

65
00:02:08,640 --> 00:02:13,680
variables that's where the buffer that's

66
00:02:10,800 --> 00:02:16,000
going to overflow is going to be stored

67
00:02:13,680 --> 00:02:18,000
so if we look at just some simple code

68
00:02:16,000 --> 00:02:20,480
here you might recognize some things

69
00:02:18,000 --> 00:02:22,400
like okay this refers to globals buff

70
00:02:20,480 --> 00:02:24,720
one and buff two these are outside of a

71
00:02:22,400 --> 00:02:26,720
function they must be globals you might

72
00:02:24,720 --> 00:02:28,080
see this buff three that's inside of a

73
00:02:26,720 --> 00:02:30,239
function that looks like a local

74
00:02:28,080 --> 00:02:32,480
variable and then you see that this

75
00:02:30,239 --> 00:02:35,680
malloc is being used to allocate eight

76
00:02:32,480 --> 00:02:37,760
bytes of memory from the heap and take

77
00:02:35,680 --> 00:02:40,239
the pointer and store it into this buff

78
00:02:37,760 --> 00:02:42,720
four so where are all of these things

79
00:02:40,239 --> 00:02:44,879
stored and you know which piece is used

80
00:02:42,720 --> 00:02:46,800
in using the stack

81
00:02:44,879 --> 00:02:48,560
so for instance these buff one and buff

82
00:02:46,800 --> 00:02:49,760
two these are globals and they're not

83
00:02:48,560 --> 00:02:52,080
going to be on the stack they're going

84
00:02:49,760 --> 00:02:53,519
to be in special global areas so for

85
00:02:52,080 --> 00:02:55,519
instance there turns out there's

86
00:02:53,519 --> 00:02:57,440
different areas typically for zero

87
00:02:55,519 --> 00:03:00,159
initialized global variables

88
00:02:57,440 --> 00:03:03,120
buff one in this case and also there are

89
00:03:00,159 --> 00:03:05,519
areas for non-zero initialized global

90
00:03:03,120 --> 00:03:08,080
variables so buff 2 is set to 0 1 2 3 4

91
00:03:05,519 --> 00:03:10,239
5 6 7 and therefore it's going to go

92
00:03:08,080 --> 00:03:11,840
into a different memory region now buff

93
00:03:10,239 --> 00:03:14,000
3 and before are what we're going to

94
00:03:11,840 --> 00:03:16,319
care about in this section these are

95
00:03:14,000 --> 00:03:19,360
local variables and local variables are

96
00:03:16,319 --> 00:03:20,879
stored on the stack so buff three is

97
00:03:19,360 --> 00:03:22,319
going to be eight bytes allocated on the

98
00:03:20,879 --> 00:03:24,640
stack and buff four is going to be

99
00:03:22,319 --> 00:03:26,959
whatever the pointer with is if it's 32

100
00:03:24,640 --> 00:03:29,519
bit system it's 32 bits if it's a 64-bit

101
00:03:26,959 --> 00:03:31,680
system it's 64 bits and then this malloc

102
00:03:29,519 --> 00:03:33,680
is going to allocate 8 bytes of memory

103
00:03:31,680 --> 00:03:35,360
on the heap it's going to return a

104
00:03:33,680 --> 00:03:37,920
pointer to that memory and it's going to

105
00:03:35,360 --> 00:03:39,760
store it into the local variable buff 4

106
00:03:37,920 --> 00:03:41,920
which is on the stack so but 4 is on the

107
00:03:39,760 --> 00:03:42,720
stack the data it points to is on the

108
00:03:41,920 --> 00:03:44,480
heap

109
00:03:42,720 --> 00:03:46,799
so we could visualize that like this

110
00:03:44,480 --> 00:03:49,280
buff 1 is zero initialized and there's a

111
00:03:46,799 --> 00:03:51,519
special area for zero or uninitialized

112
00:03:49,280 --> 00:03:53,680
global memory buff two on the other hand

113
00:03:51,519 --> 00:03:56,720
because it was initialized is going to

114
00:03:53,680 --> 00:03:58,560
go into a special area for globals that

115
00:03:56,720 --> 00:04:00,319
are initialized and I'm actually showing

116
00:03:58,560 --> 00:04:02,480
this in green the same way as code here

117
00:04:00,319 --> 00:04:05,120
just to kind of indicate that typically

118
00:04:02,480 --> 00:04:06,720
because you have specific initialization

119
00:04:05,120 --> 00:04:09,280
those specific values are going to be

120
00:04:06,720 --> 00:04:11,280
compiled into the binary on disk so all

121
00:04:09,280 --> 00:04:13,439
of this green stuff came from the actual

122
00:04:11,280 --> 00:04:16,239
compiled binary on disk whereas this

123
00:04:13,439 --> 00:04:18,160
pink stuff is actually just allocated at

124
00:04:16,239 --> 00:04:20,239
execution time but again the part that

125
00:04:18,160 --> 00:04:22,800
we really care about is this buff three

126
00:04:20,239 --> 00:04:24,880
and buff four these are local variables

127
00:04:22,800 --> 00:04:26,400
and they exist on the stack so buff

128
00:04:24,880 --> 00:04:29,280
three will be eight bytes of data on the

129
00:04:26,400 --> 00:04:31,919
stack but four is a pointer on the stack

130
00:04:29,280 --> 00:04:33,840
and it points to after this malloc like

131
00:04:31,919 --> 00:04:35,440
initially it points to nothing but after

132
00:04:33,840 --> 00:04:37,840
the malloc it points to these eight

133
00:04:35,440 --> 00:04:39,759
bytes on the heap but before we get too

134
00:04:37,840 --> 00:04:41,600
much into this we need to establish some

135
00:04:39,759 --> 00:04:43,360
conventions that we're going to use for

136
00:04:41,600 --> 00:04:45,040
diagrams in this class

137
00:04:43,360 --> 00:04:47,759
if you see gray that is meant to

138
00:04:45,040 --> 00:04:50,720
indicate it is uninitialized data blue

139
00:04:47,759 --> 00:04:52,960
is initialized red is ACID attacker

140
00:04:50,720 --> 00:04:55,199
controlled input data and blue

141
00:04:52,960 --> 00:04:58,800
transitioning into red means that it's

142
00:04:55,199 --> 00:05:01,120
semi attacker control data or SACI data

143
00:04:58,800 --> 00:05:03,280
now you will see that there are frequent

144
00:05:01,120 --> 00:05:05,360
cases where an attacker may not exactly

145
00:05:03,280 --> 00:05:07,520
control everything about the data but

146
00:05:05,360 --> 00:05:09,600
still even with SACI data instead of

147
00:05:07,520 --> 00:05:11,520
ACID data an attacker can still achieve

148
00:05:09,600 --> 00:05:13,919
their goals most of the time

149
00:05:11,520 --> 00:05:16,320
so if this was 64 bytes then when we're

150
00:05:13,919 --> 00:05:18,320
drawing it as a stack diagram we will

151
00:05:16,320 --> 00:05:20,240
place low address as low and high

152
00:05:18,320 --> 00:05:21,919
address is high this is a convention

153
00:05:20,240 --> 00:05:24,479
that's common in computer architecture

154
00:05:21,919 --> 00:05:25,759
books and is also used in other ost-2

155
00:05:24,479 --> 00:05:27,520
classes

156
00:05:25,759 --> 00:05:29,440
now in this convention if we assume that

157
00:05:27,520 --> 00:05:31,600
we've got a little andian architecture

158
00:05:29,440 --> 00:05:33,680
we're going to draw the little end or

159
00:05:31,600 --> 00:05:36,000
the least significant byte here to the

160
00:05:33,680 --> 00:05:38,479
right and the big end or most

161
00:05:36,000 --> 00:05:40,880
significant byte to the left okay well

162
00:05:38,479 --> 00:05:42,720
we know that a stack data structure has

163
00:05:40,880 --> 00:05:44,639
data growing from the bottom of the

164
00:05:42,720 --> 00:05:46,320
stack to the top of the stack and

165
00:05:44,639 --> 00:05:48,160
therefore with our low dresses low and

166
00:05:46,320 --> 00:05:50,320
high address as high convention we're

167
00:05:48,160 --> 00:05:52,800
going to be seeing the stack growing

168
00:05:50,320 --> 00:05:54,639
downward so eight bytes at a time for

169
00:05:52,800 --> 00:05:56,720
instance on a 64-bit architecture are

170
00:05:54,639 --> 00:05:58,560
typically pushed onto the stack and that

171
00:05:56,720 --> 00:06:00,960
means it's going to grow towards

172
00:05:58,560 --> 00:06:03,120
progressively lower things and the thing

173
00:06:00,960 --> 00:06:05,440
at the lowest address is the top of the

174
00:06:03,120 --> 00:06:07,600
stack and the thing at the top of the

175
00:06:05,440 --> 00:06:09,600
diagram is the bottom of the stack

176
00:06:07,600 --> 00:06:13,039
all right so stack grows towards low

177
00:06:09,600 --> 00:06:14,400
addresses but memory rights grow towards

178
00:06:13,039 --> 00:06:16,240
high addresses

179
00:06:14,400 --> 00:06:18,880
so any sort of right is going to

180
00:06:16,240 --> 00:06:21,520
overwrite this direction going up on

181
00:06:18,880 --> 00:06:25,280
this diagram in this orientation

182
00:06:21,520 --> 00:06:26,240
so for instance if we had a 64 byte

183
00:06:25,280 --> 00:06:28,160
buffer

184
00:06:26,240 --> 00:06:31,360
the indexing which is going to be used

185
00:06:28,160 --> 00:06:34,560
on our stack diagrams is index 0 and x1

186
00:06:31,360 --> 00:06:36,479
etc all the way up through index 63 up

187
00:06:34,560 --> 00:06:39,759
here at the top

188
00:06:36,479 --> 00:06:41,440
and so if we had some value like hex one

189
00:06:39,759 --> 00:06:43,840
one two two three three four four

190
00:06:41,440 --> 00:06:45,120
etcetera if this was a little endian

191
00:06:43,840 --> 00:06:46,880
architecture meaning the least

192
00:06:45,120 --> 00:06:48,800
significant byte is here on the right

193
00:06:46,880 --> 00:06:51,199
and the most significant byte is here on

194
00:06:48,800 --> 00:06:52,800
the left then we would convert to this

195
00:06:51,199 --> 00:06:55,360
eight individual bytes if we were

196
00:06:52,800 --> 00:06:57,360
interpreting it as a keyword or a

197
00:06:55,360 --> 00:06:59,039
one signed long long

198
00:06:57,360 --> 00:07:00,720
that would be interpreted as an eight

199
00:06:59,039 --> 00:07:02,319
byte value of hex one two three four

200
00:07:00,720 --> 00:07:04,479
five etc

201
00:07:02,319 --> 00:07:06,639
so you know this is just uh trying to

202
00:07:04,479 --> 00:07:08,560
remind us you know how little endianness

203
00:07:06,639 --> 00:07:10,400
works on architecture

204
00:07:08,560 --> 00:07:13,120
on the other hand if you had something

205
00:07:10,400 --> 00:07:14,720
like a string instead of a number well

206
00:07:13,120 --> 00:07:18,880
the least significant byte of a string

207
00:07:14,720 --> 00:07:19,919
is over here so slash h o m e slash u s

208
00:07:18,880 --> 00:07:22,160
e r

209
00:07:19,919 --> 00:07:25,039
so this would for instance be the string

210
00:07:22,160 --> 00:07:27,599
slash home slash user slash sbo

211
00:07:25,039 --> 00:07:29,840
so again the little endian-ness means

212
00:07:27,599 --> 00:07:32,400
that it's going to go from the right to

213
00:07:29,840 --> 00:07:34,080
the left bottom to the top in stack

214
00:07:32,400 --> 00:07:35,680
diagrams

215
00:07:34,080 --> 00:07:37,360
now when it comes to what's going on

216
00:07:35,680 --> 00:07:39,199
behind the scenes there's a variety of

217
00:07:37,360 --> 00:07:41,520
things that can be stored on the stack

218
00:07:39,199 --> 00:07:43,759
and it's going to depend on the computer

219
00:07:41,520 --> 00:07:45,599
architecture the calling conventions

220
00:07:43,759 --> 00:07:47,759
that are used by the particular

221
00:07:45,599 --> 00:07:50,000
compilers and code that's being compiled

222
00:07:47,759 --> 00:07:52,639
and we cover this in other classes such

223
00:07:50,000 --> 00:07:54,240
as assembly classes for purposes of this

224
00:07:52,639 --> 00:07:56,080
class the only thing that you really

225
00:07:54,240 --> 00:07:58,240
really need to know is the fact that

226
00:07:56,080 --> 00:08:00,160
there is a return address generally

227
00:07:58,240 --> 00:08:02,319
stored on the stack somewhere on most

228
00:08:00,160 --> 00:08:04,160
architectures not necessarily in leaf

229
00:08:02,319 --> 00:08:05,599
functions but pretty much every

230
00:08:04,160 --> 00:08:07,360
architecture is going to have something

231
00:08:05,599 --> 00:08:10,400
like a return address and the return

232
00:08:07,360 --> 00:08:12,720
address is the address of code where

233
00:08:10,400 --> 00:08:16,080
code should return when you return out

234
00:08:12,720 --> 00:08:18,160
of a C function right so in C we call a

235
00:08:16,080 --> 00:08:20,000
function in order to jump in and start

236
00:08:18,160 --> 00:08:22,400
executing the code and when we're done

237
00:08:20,000 --> 00:08:25,520
with function we return back from that

238
00:08:22,400 --> 00:08:27,199
function and so somewhere somehow

239
00:08:25,520 --> 00:08:29,039
the architecture has to store like how

240
00:08:27,199 --> 00:08:31,360
do we get back to the place that we came

241
00:08:29,039 --> 00:08:33,200
from when this call was issued and we

242
00:08:31,360 --> 00:08:34,959
call that the return address which is

243
00:08:33,200 --> 00:08:37,360
stored on the stack

244
00:08:34,959 --> 00:08:39,440
so if the stack is organized into like a

245
00:08:37,360 --> 00:08:41,760
sequence of frames for function one

246
00:08:39,440 --> 00:08:44,399
calls function two calls function three

247
00:08:41,760 --> 00:08:46,320
then on our stack diagrams the frames

248
00:08:44,399 --> 00:08:48,240
for earlier functions will be on the top

249
00:08:46,320 --> 00:08:50,720
and later functions and ultimately the

250
00:08:48,240 --> 00:08:52,240
leaf function will be on the bottom and

251
00:08:50,720 --> 00:08:53,839
so again there's a variety of things

252
00:08:52,240 --> 00:08:55,920
that could be on the stack and it really

253
00:08:53,839 --> 00:08:57,600
all just depends but what I want to say

254
00:08:55,920 --> 00:08:59,680
is that return address is going to be

255
00:08:57,600 --> 00:09:01,360
the very key and critical thing this is

256
00:08:59,680 --> 00:09:03,600
what attackers are going to be trying to

257
00:09:01,360 --> 00:09:05,440
corrupt with stack buffer overflows but

258
00:09:03,600 --> 00:09:07,360
there's also things like local variables

259
00:09:05,440 --> 00:09:09,360
and attackers can get a lot of benefit

260
00:09:07,360 --> 00:09:11,839
from corrupting local variables

261
00:09:09,360 --> 00:09:14,720
there's saved registers in terms of

262
00:09:11,839 --> 00:09:16,560
callee and caller saved registers

263
00:09:14,720 --> 00:09:18,160
there's function parameters

264
00:09:16,560 --> 00:09:19,760
on some architectures which might get

265
00:09:18,160 --> 00:09:21,920
saved on the stack and it could depend

266
00:09:19,760 --> 00:09:23,839
on how many parameters are passed to a

267
00:09:21,920 --> 00:09:25,600
function and so forth

268
00:09:23,839 --> 00:09:27,839
and then you know again return addresses

269
00:09:25,600 --> 00:09:30,240
so this is the type of things that we

270
00:09:27,839 --> 00:09:32,080
could have I left out various things

271
00:09:30,240 --> 00:09:33,519
depending on architecture but in general

272
00:09:32,080 --> 00:09:35,200
all architectures are going to have this

273
00:09:33,519 --> 00:09:37,120
kind of stuff and the one we really

274
00:09:35,200 --> 00:09:38,800
really care about is that return address

275
00:09:37,120 --> 00:09:40,000
that's going to be a good target for an

276
00:09:38,800 --> 00:09:42,000
attacker

277
00:09:40,000 --> 00:09:43,600
so those are our conventions let's get

278
00:09:42,000 --> 00:09:47,040
back to it what is a stack buffer

279
00:09:43,600 --> 00:09:49,360
overflow it's when too much ACID data is

280
00:09:47,040 --> 00:09:50,720
copied into a stack buffer overflowing

281
00:09:49,360 --> 00:09:52,959
its bounds

282
00:09:50,720 --> 00:09:55,279
so let's you know let's consider this

283
00:09:52,959 --> 00:09:57,120
you know visualize this let's say we had

284
00:09:55,279 --> 00:09:59,440
a memcpy and it's

285
00:09:57,120 --> 00:10:02,000
copying into vulnerable buffer it's

286
00:09:59,440 --> 00:10:04,079
copying from ACID buffer and it has an

287
00:10:02,000 --> 00:10:05,839
attacker controlled length so this would

288
00:10:04,079 --> 00:10:08,000
look something like this you're copying

289
00:10:05,839 --> 00:10:10,240
the data from here and it's flowing and

290
00:10:08,000 --> 00:10:12,399
it's flowing and eventually it's

291
00:10:10,240 --> 00:10:14,720
overflowing the bounds of the vulnerable

292
00:10:12,399 --> 00:10:16,959
buffer and it's consequently smashing

293
00:10:14,720 --> 00:10:19,120
the return address on the stack and that

294
00:10:16,959 --> 00:10:20,640
gives the attacker some benefit they

295
00:10:19,120 --> 00:10:22,880
overwrite the return address and

296
00:10:20,640 --> 00:10:24,800
therefore when the function returns it's

297
00:10:22,880 --> 00:10:26,320
going to go to some attacker controlled

298
00:10:24,800 --> 00:10:28,399
location

299
00:10:26,320 --> 00:10:30,720
so what are the common causes of stack

300
00:10:28,399 --> 00:10:32,720
buffer overflows well we've got two root

301
00:10:30,720 --> 00:10:34,560
causes we've got the sweet potato and

302
00:10:32,720 --> 00:10:36,720
the carrot these being the only roots

303
00:10:34,560 --> 00:10:39,120
available in the emoji set at the time

304
00:10:36,720 --> 00:10:41,200
so in the sweet potato case we've got

305
00:10:39,120 --> 00:10:43,200
unsafe or weakly bounded common

306
00:10:41,200 --> 00:10:45,760
functions like memcpy strcpy

307
00:10:43,200 --> 00:10:47,839
string cat s printf etc these are things

308
00:10:45,760 --> 00:10:49,680
that typically you know are either

309
00:10:47,839 --> 00:10:52,079
explicitly for purposes of copying

310
00:10:49,680 --> 00:10:54,000
memory or you know implicitly doing

311
00:10:52,079 --> 00:10:56,720
things like string operations which will

312
00:10:54,000 --> 00:10:57,920
lead to copying memory there's also very

313
00:10:56,720 --> 00:11:00,000
frequently

314
00:10:57,920 --> 00:11:02,240
wrappers where you know some programmer

315
00:11:00,000 --> 00:11:04,000
wants to do a custom memcpy that's

316
00:11:02,240 --> 00:11:06,000
just for their particular struct data

317
00:11:04,000 --> 00:11:07,839
type or something like that and at the

318
00:11:06,000 --> 00:11:10,399
end of the day they may be copying one

319
00:11:07,839 --> 00:11:11,839
struct to another struct but behind the

320
00:11:10,399 --> 00:11:13,600
scenes you know it's usually backed by

321
00:11:11,839 --> 00:11:14,959
something like a memcpy so you have to

322
00:11:13,600 --> 00:11:17,279
watch out for these sort of wrapper

323
00:11:14,959 --> 00:11:19,279
functions as well

324
00:11:17,279 --> 00:11:21,680
then we've got the other caret cause

325
00:11:19,279 --> 00:11:25,120
which is sequential data rights within a

326
00:11:21,680 --> 00:11:27,200
loop that is an asset exit condition so

327
00:11:25,120 --> 00:11:28,720
data rights so right here means like

328
00:11:27,200 --> 00:11:31,040
it's copying it's copying from one

329
00:11:28,720 --> 00:11:32,640
location to a different memory location

330
00:11:31,040 --> 00:11:34,399
usually not with something like a mem

331
00:11:32,640 --> 00:11:36,399
copy usually something just like you

332
00:11:34,399 --> 00:11:38,079
know assigning to

333
00:11:36,399 --> 00:11:40,480
some dereferenced pointer or something

334
00:11:38,079 --> 00:11:42,560
like that so you've got rights and

335
00:11:40,480 --> 00:11:45,360
they're within a loop so it effectively

336
00:11:42,560 --> 00:11:47,519
is a memory copy operation and when that

337
00:11:45,360 --> 00:11:48,880
loop exit condition is attacker

338
00:11:47,519 --> 00:11:50,560
controlled that's when you're going to

339
00:11:48,880 --> 00:11:52,240
get into problems because they can

340
00:11:50,560 --> 00:11:55,360
potentially control that loop to keep

341
00:11:52,240 --> 00:11:57,680
copying too much and ultimately overflow

342
00:11:55,360 --> 00:11:59,600
buffer so let's see a trivial example of

343
00:11:57,680 --> 00:12:01,440
a stack buffer overflow we're going to

344
00:11:59,600 --> 00:12:03,279
see a whole bunch of real examples

345
00:12:01,440 --> 00:12:05,680
shortly but you've got to always start

346
00:12:03,279 --> 00:12:08,399
with something trivial so in this case

347
00:12:05,680 --> 00:12:10,720
we have main which is taking a argument

348
00:12:08,399 --> 00:12:13,760
count and an argument vector and we've

349
00:12:10,720 --> 00:12:15,760
got buff which is what it's a stack

350
00:12:13,760 --> 00:12:18,240
buffer it's a stack buffer because it's

351
00:12:15,760 --> 00:12:20,639
a local variable and this buff is only

352
00:12:18,240 --> 00:12:23,360
eight bytes big but we are going to use

353
00:12:20,639 --> 00:12:25,600
an unbounded strcpy function to

354
00:12:23,360 --> 00:12:26,399
copy an arbitrary number of bytes from

355
00:12:25,600 --> 00:12:29,920
the

356
00:12:26,399 --> 00:12:31,760
1th artwonth argument of the arg vector

357
00:12:29,920 --> 00:12:34,160
and that's going to be copied into buff

358
00:12:31,760 --> 00:12:37,360
so that's just a arbitrary size string

359
00:12:34,160 --> 00:12:39,680
could be 256 bytes and that'll be copied

360
00:12:37,360 --> 00:12:41,760
into buff which is only eight bytes so

361
00:12:39,680 --> 00:12:43,920
which case is this which common root

362
00:12:41,760 --> 00:12:46,240
cause case is this this is the sweet

363
00:12:43,920 --> 00:12:48,399
potato case sweet potato is these weekly

364
00:12:46,240 --> 00:12:50,639
bounded functions so what does this look

365
00:12:48,399 --> 00:12:52,720
like when we visualize it here is our

366
00:12:50,639 --> 00:12:54,880
stack diagram visualization we're using

367
00:12:52,720 --> 00:12:57,279
by convention in this section high

368
00:12:54,880 --> 00:12:59,120
address is high low address is low

369
00:12:57,279 --> 00:13:01,519
least significant byte to the right most

370
00:12:59,120 --> 00:13:03,600
significant byte to the left

371
00:13:01,519 --> 00:13:06,399
so when we have the argument vector

372
00:13:03,600 --> 00:13:08,480
initialized it turns out that argv is

373
00:13:06,399 --> 00:13:10,880
stored somewhere on the stack so this

374
00:13:08,480 --> 00:13:12,959
arc v of zero is actually going to be

375
00:13:10,880 --> 00:13:15,040
SACI data as shown here it's not

376
00:13:12,959 --> 00:13:17,360
completely attacker controlled because

377
00:13:15,040 --> 00:13:19,360
if we you know put on our nerd glasses

378
00:13:17,360 --> 00:13:21,600
and look at it very carefully we would

379
00:13:19,360 --> 00:13:24,480
say that attacker cannot completely

380
00:13:21,600 --> 00:13:25,839
control this path because if they could

381
00:13:24,480 --> 00:13:27,360
then that would mean they could put it

382
00:13:25,839 --> 00:13:29,519
anywhere on the system and if they could

383
00:13:27,360 --> 00:13:31,519
do that then that would suggest that

384
00:13:29,519 --> 00:13:33,600
they already had administrative rights

385
00:13:31,519 --> 00:13:35,839
and if they had that then on most unix

386
00:13:33,600 --> 00:13:37,519
type systems they've already won so

387
00:13:35,839 --> 00:13:39,199
we're going to say that it's SACI data

388
00:13:37,519 --> 00:13:42,320
and in my particular case I was running

389
00:13:39,199 --> 00:13:44,800
it from slash home slash user slash sbo

390
00:13:42,320 --> 00:13:47,120
so rv 0 by convention usually is going

391
00:13:44,800 --> 00:13:49,279
to be the path to the executable itself

392
00:13:47,120 --> 00:13:51,279
and then arc v1 is going to be the

393
00:13:49,279 --> 00:13:53,839
actual command line argument that's

394
00:13:51,279 --> 00:13:56,160
passed to that particular executable

395
00:13:53,839 --> 00:13:58,480
here now you can see that rv1 is

396
00:13:56,160 --> 00:14:01,040
completely attacker-controlled input so

397
00:13:58,480 --> 00:14:03,440
they can put whatever they want there

398
00:14:01,040 --> 00:14:04,880
then once other memory is initialized

399
00:14:03,440 --> 00:14:06,320
we're just going to give that a dot dot

400
00:14:04,880 --> 00:14:07,920
dot and say you know this exists

401
00:14:06,320 --> 00:14:09,760
somewhere on the stack some other stuff

402
00:14:07,920 --> 00:14:12,399
is going to be initialized that's blue

403
00:14:09,760 --> 00:14:14,959
for initialized and then finally main is

404
00:14:12,399 --> 00:14:17,440
called now a side effect of calling a

405
00:14:14,959 --> 00:14:20,480
function as we said before is that you

406
00:14:17,440 --> 00:14:23,040
have the return address to get back to

407
00:14:20,480 --> 00:14:25,839
wherever you came from pushed onto the

408
00:14:23,040 --> 00:14:28,800
stack and so even though you only wrote

409
00:14:25,839 --> 00:14:30,720
the main function inside of your code

410
00:14:28,800 --> 00:14:32,160
something somewhere has to call and

411
00:14:30,720 --> 00:14:34,480
invoke that main and that's something

412
00:14:32,160 --> 00:14:36,560
somewhere that return back out of main

413
00:14:34,480 --> 00:14:38,560
whatever that function is that's going

414
00:14:36,560 --> 00:14:41,120
to have the return address back to that

415
00:14:38,560 --> 00:14:43,199
function pushed onto the stack okay so

416
00:14:41,120 --> 00:14:45,519
now we assume main has been called first

417
00:14:43,199 --> 00:14:48,480
thing it does is it allocates space for

418
00:14:45,519 --> 00:14:51,839
that local variable the buff eight

419
00:14:48,480 --> 00:14:53,440
and so buff is eight bytes on the stack

420
00:14:51,839 --> 00:14:55,839
that is going to be allocated but

421
00:14:53,440 --> 00:14:58,240
uninitialized so it's just reserved but

422
00:14:55,839 --> 00:14:59,360
it's not set to anything yet and it just

423
00:14:58,240 --> 00:15:01,440
assumes that the code needs to

424
00:14:59,360 --> 00:15:03,600
initialize that at some point

425
00:15:01,440 --> 00:15:07,120
then next comes this dangerous string

426
00:15:03,600 --> 00:15:09,440
copy and that's going to copy from rv1

427
00:15:07,120 --> 00:15:11,680
to buff and you can see that rgb1 is

428
00:15:09,440 --> 00:15:14,000
bigger than buff so what happens when we

429
00:15:11,680 --> 00:15:16,320
start that copy well attacker controlled

430
00:15:14,000 --> 00:15:19,519
input gets put in and oh no it's

431
00:15:16,320 --> 00:15:21,440
smashing the stack for fun and profit

432
00:15:19,519 --> 00:15:23,040
and the net result of that is that all

433
00:15:21,440 --> 00:15:25,199
of a sudden that return address which

434
00:15:23,040 --> 00:15:26,880
was supposed to return back to whatever

435
00:15:25,199 --> 00:15:28,720
function called main

436
00:15:26,880 --> 00:15:31,040
now points at an arbitrary

437
00:15:28,720 --> 00:15:33,279
attacker-controlled value and that means

438
00:15:31,040 --> 00:15:35,360
the attacker gets to return back to code

439
00:15:33,279 --> 00:15:37,040
anywhere they want on the system of

440
00:15:35,360 --> 00:15:38,720
course it's ultimately up to the

441
00:15:37,040 --> 00:15:41,519
attacker to figure out where is

442
00:15:38,720 --> 00:15:43,279
profitable to return to and that's not

443
00:15:41,519 --> 00:15:45,519
the topic for this class that would be a

444
00:15:43,279 --> 00:15:47,040
future exploitation class in this class

445
00:15:45,519 --> 00:15:48,480
we're just going to try to figure out

446
00:15:47,040 --> 00:15:50,320
where are these vulnerabilities that

447
00:15:48,480 --> 00:15:51,920
would allow these sort of attacks

448
00:15:50,320 --> 00:15:54,560
so stack buffer overflows you know

449
00:15:51,920 --> 00:15:56,160
they're very old type of vulnerability

450
00:15:54,560 --> 00:15:58,399
you know it's not just smashing the

451
00:15:56,160 --> 00:16:00,480
stack for fun and profit 1996 type old

452
00:15:58,399 --> 00:16:03,040
that I cited before we're talking like

453
00:16:00,480 --> 00:16:05,680
1980s old and really you know their

454
00:16:03,040 --> 00:16:08,480
citations back to the to the 70s

455
00:16:05,680 --> 00:16:11,040
so you know the very first worm that was

456
00:16:08,480 --> 00:16:13,920
ever released on the proto internet the

457
00:16:11,040 --> 00:16:16,880
arpanet was the morris worm released in

458
00:16:13,920 --> 00:16:19,440
1988 and this was a stack buffer

459
00:16:16,880 --> 00:16:22,160
overflow vulnerability it exploited a

460
00:16:19,440 --> 00:16:24,480
stack overflow in the finger daemon

461
00:16:22,160 --> 00:16:26,399
which would typically run on most unix

462
00:16:24,480 --> 00:16:28,079
systems it was a way of querying

463
00:16:26,399 --> 00:16:29,440
information about other users on the

464
00:16:28,079 --> 00:16:31,360
system

465
00:16:29,440 --> 00:16:34,000
so this has been going on for a very

466
00:16:31,360 --> 00:16:35,440
long time and so the point here is of

467
00:16:34,000 --> 00:16:37,680
saying that it's going on for a very

468
00:16:35,440 --> 00:16:39,519
long time is that you know

469
00:16:37,680 --> 00:16:42,079
if everybody's still making these sort

470
00:16:39,519 --> 00:16:44,079
of mistakes in this day and age after

471
00:16:42,079 --> 00:16:45,519
you know well more than 30 years that

472
00:16:44,079 --> 00:16:47,120
means you know it's not fundamentally

473
00:16:45,519 --> 00:16:49,120
the programmer's problem it's the

474
00:16:47,120 --> 00:16:51,040
programming language problem that's why

475
00:16:49,120 --> 00:16:52,959
we're not trying to demonize people who

476
00:16:51,040 --> 00:16:54,399
make these sort of mistakes most people

477
00:16:52,959 --> 00:16:56,480
have never learned anything about them

478
00:16:54,399 --> 00:16:58,880
so how could they know you know not to

479
00:16:56,480 --> 00:17:00,480
have this kind of thing occurring and so

480
00:16:58,880 --> 00:17:02,800
here we're really just trying to focus

481
00:17:00,480 --> 00:17:04,959
on how do we recognize these kind of

482
00:17:02,800 --> 00:17:06,880
mistakes and avoid them

483
00:17:04,959 --> 00:17:11,160
so now let's go look at some real

484
00:17:06,880 --> 00:17:11,160
examples from real vulnerabilities


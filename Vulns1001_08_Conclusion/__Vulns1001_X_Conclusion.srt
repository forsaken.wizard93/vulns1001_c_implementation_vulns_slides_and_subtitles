1
00:00:00,160 --> 00:00:03,840
well this marks the end of the class and

2
00:00:01,760 --> 00:00:05,600
I hope you've enjoyed our time together

3
00:00:03,840 --> 00:00:07,040
there's more to come in future classes

4
00:00:05,600 --> 00:00:08,800
where we cover new types of

5
00:00:07,040 --> 00:00:10,960
vulnerabilities and even this class

6
00:00:08,800 --> 00:00:13,759
itself will be upgraded over time by

7
00:00:10,960 --> 00:00:15,440
just adding new examples but let's recap

8
00:00:13,759 --> 00:00:17,279
some of the key points from this class

9
00:00:15,440 --> 00:00:18,800
shall we so we learned that this matters

10
00:00:17,279 --> 00:00:21,119
because at the end of the day real

11
00:00:18,800 --> 00:00:23,840
people are harmed by vulnerabilities in

12
00:00:21,119 --> 00:00:25,760
systems whether it's you know some

13
00:00:23,840 --> 00:00:27,840
family member getting money stolen out

14
00:00:25,760 --> 00:00:29,840
of their bank account or whether it's

15
00:00:27,840 --> 00:00:31,760
thousands or millions of people losing

16
00:00:29,840 --> 00:00:34,079
electricity because the power grid was

17
00:00:31,760 --> 00:00:36,000
taken down these sort of vulnerabilities

18
00:00:34,079 --> 00:00:39,280
do cause harm to people

19
00:00:36,000 --> 00:00:41,360
we learned about ACID ACID burns

20
00:00:39,280 --> 00:00:43,600
ACID flows through programs corrupting

21
00:00:41,360 --> 00:00:46,239
everything it touches and it's your job

22
00:00:43,600 --> 00:00:49,200
to neutralize sanitize or otherwise stop

23
00:00:46,239 --> 00:00:50,960
ACID from causing a problem or if you're

24
00:00:49,200 --> 00:00:53,440
in a different line of work it's your

25
00:00:50,960 --> 00:00:54,879
job to hack the planet we learned about

26
00:00:53,440 --> 00:00:56,800
all of this different sort of

27
00:00:54,879 --> 00:00:59,120
attacker-controlled input that can be

28
00:00:56,800 --> 00:01:00,800
fed into programs you know it's not just

29
00:00:59,120 --> 00:01:02,719
the simple things you think of you know

30
00:01:00,800 --> 00:01:04,159
maybe files or something like that it's

31
00:01:02,719 --> 00:01:05,840
all sorts of things it's you know

32
00:01:04,159 --> 00:01:08,080
communicating with a peripheral when

33
00:01:05,840 --> 00:01:09,920
you're in the kernel it's a hyper call

34
00:01:08,080 --> 00:01:12,799
when you're a hypervisor maybe a

35
00:01:09,920 --> 00:01:14,479
debugging interface in some contexts is

36
00:01:12,799 --> 00:01:17,200
actually allowing for attack control

37
00:01:14,479 --> 00:01:18,400
data that you didn't intend so basically

38
00:01:17,200 --> 00:01:20,640
you know again goes along with

39
00:01:18,400 --> 00:01:23,200
programming paranoid just kind of assume

40
00:01:20,640 --> 00:01:24,720
that everything that is fed into a

41
00:01:23,200 --> 00:01:26,720
program from the outside is

42
00:01:24,720 --> 00:01:28,400
attacker-controlled input

43
00:01:26,720 --> 00:01:29,920
we learned about how you know you might

44
00:01:28,400 --> 00:01:32,640
be in this class because you're a

45
00:01:29,920 --> 00:01:34,720
developer trying to secure your software

46
00:01:32,640 --> 00:01:36,960
or you might be here because you want to

47
00:01:34,720 --> 00:01:39,119
hunt vulnerabilities but at the end of

48
00:01:36,960 --> 00:01:41,119
the day this information is exactly the

49
00:01:39,119 --> 00:01:42,640
same and if you're a developer i'm

50
00:01:41,119 --> 00:01:43,840
teaching the vulnerability hunters if

51
00:01:42,640 --> 00:01:46,240
you're a vulnerability hunter i'm

52
00:01:43,840 --> 00:01:48,399
teaching the developers so it's up to

53
00:01:46,240 --> 00:01:50,880
each of you to figure out you know how

54
00:01:48,399 --> 00:01:53,439
can you do your job the best

55
00:01:50,880 --> 00:01:55,680
we learned about the sploity sense that

56
00:01:53,439 --> 00:01:58,240
supernatural power that some people seem

57
00:01:55,680 --> 00:02:00,159
to possess that gives them an intuition

58
00:01:58,240 --> 00:02:02,320
about when something's about to go wrong

59
00:02:00,159 --> 00:02:04,079
and when there's danger in a code base

60
00:02:02,320 --> 00:02:05,759
but we also learned that there's nothing

61
00:02:04,079 --> 00:02:07,119
supernatural about the spleidy sense and

62
00:02:05,759 --> 00:02:08,479
that at the end of the day it's just

63
00:02:07,119 --> 00:02:10,399
pattern recognition

64
00:02:08,479 --> 00:02:12,720
that's what our brains are supposed to

65
00:02:10,399 --> 00:02:15,040
do supposed to recognize patterns and so

66
00:02:12,720 --> 00:02:17,840
if you've seen a hundred stack buffer

67
00:02:15,040 --> 00:02:20,640
overflows then it's pretty easy to spot

68
00:02:17,840 --> 00:02:22,480
the 101st we learned about exploit

69
00:02:20,640 --> 00:02:24,720
primitives and how an attacker will

70
00:02:22,480 --> 00:02:26,560
combine different things like adjacent

71
00:02:24,720 --> 00:02:28,319
data overwrites or information

72
00:02:26,560 --> 00:02:30,640
disclosure and they'll put all of these

73
00:02:28,319 --> 00:02:32,239
primitive little elements together and

74
00:02:30,640 --> 00:02:34,560
subsequently they'll be able to create a

75
00:02:32,239 --> 00:02:36,080
full exploit chain learn about exploit

76
00:02:34,560 --> 00:02:37,680
chains and that's that idea of the

77
00:02:36,080 --> 00:02:39,920
different attacker primitives combined

78
00:02:37,680 --> 00:02:42,160
together and the reason we mentioned the

79
00:02:39,920 --> 00:02:44,239
exploit chains is just to say that you

80
00:02:42,160 --> 00:02:46,080
know any given little vulnerability

81
00:02:44,239 --> 00:02:47,760
might not seem important at the time

82
00:02:46,080 --> 00:02:50,080
might seem like oh I don't see how that

83
00:02:47,760 --> 00:02:52,720
could matter that you know can't in and

84
00:02:50,080 --> 00:02:54,800
of itself allow for exploitability but

85
00:02:52,720 --> 00:02:56,720
leaving around those little bugs just

86
00:02:54,800 --> 00:02:58,720
gives an attacker an opportunity to

87
00:02:56,720 --> 00:03:00,480
chain them together with other bugs in

88
00:02:58,720 --> 00:03:01,840
order to ultimately achieve their goals

89
00:03:00,480 --> 00:03:04,319
and that's why it's very important to

90
00:03:01,840 --> 00:03:06,319
close down even the innocuous seeming

91
00:03:04,319 --> 00:03:08,879
bugs because at the end of the day

92
00:03:06,319 --> 00:03:11,360
exploit engineering is just another type

93
00:03:08,879 --> 00:03:13,760
of engineering it's just a job for some

94
00:03:11,360 --> 00:03:16,319
people to sit around every day you know

95
00:03:13,760 --> 00:03:18,000
at their nine-to-five job and create an

96
00:03:16,319 --> 00:03:20,159
exploit chaining together these

97
00:03:18,000 --> 00:03:21,519
different little pieces of capabilities

98
00:03:20,159 --> 00:03:23,840
they get from a bunch of different

99
00:03:21,519 --> 00:03:26,080
vulnerabilities and so you know we

100
00:03:23,840 --> 00:03:28,640
covered exploits in this class only to

101
00:03:26,080 --> 00:03:30,640
give you an intuition that even if you

102
00:03:28,640 --> 00:03:32,480
may not understand how a particular

103
00:03:30,640 --> 00:03:34,480
vulnerability is exploitable there are

104
00:03:32,480 --> 00:03:36,560
people who it's just their job to

105
00:03:34,480 --> 00:03:38,319
understand that and they just do this

106
00:03:36,560 --> 00:03:40,560
very special form of software

107
00:03:38,319 --> 00:03:41,920
engineering that is exploit engineering

108
00:03:40,560 --> 00:03:44,080
and you know they're going to figure out

109
00:03:41,920 --> 00:03:45,599
a way to make it happen it's their job

110
00:03:44,080 --> 00:03:47,599
right they only get paid if they do

111
00:03:45,599 --> 00:03:50,080
their job well right so again it's

112
00:03:47,599 --> 00:03:52,000
always just the best to assume bugs are

113
00:03:50,080 --> 00:03:55,040
exploitable and close them down and kill

114
00:03:52,000 --> 00:03:57,599
them with extreme prejudice but a final

115
00:03:55,040 --> 00:03:59,680
thought defense is possible it might

116
00:03:57,599 --> 00:04:01,280
seem like you know oh it's going to be

117
00:03:59,680 --> 00:04:03,200
impossible to close down all the

118
00:04:01,280 --> 00:04:04,799
vulnerabilities everywhere but at the

119
00:04:03,200 --> 00:04:07,040
end of the day nothing ruins an

120
00:04:04,799 --> 00:04:09,599
attacker's day like a well-placed sanity

121
00:04:07,040 --> 00:04:11,120
check right we showed what the sanity

122
00:04:09,599 --> 00:04:12,799
checks looked like that actually closed

123
00:04:11,120 --> 00:04:15,360
down vulnerabilities and in a lot of

124
00:04:12,799 --> 00:04:18,560
cases it's just like a couple line fix

125
00:04:15,360 --> 00:04:20,479
right so that's easy of course not all

126
00:04:18,560 --> 00:04:22,479
sanity checks are created equal we can

127
00:04:20,479 --> 00:04:24,240
have things like the insanity checks

128
00:04:22,479 --> 00:04:26,960
where if you don't properly take

129
00:04:24,240 --> 00:04:29,040
signedness into account then you're just

130
00:04:26,960 --> 00:04:30,720
going to be you know making an attacker

131
00:04:29,040 --> 00:04:32,720
do something slightly different in order

132
00:04:30,720 --> 00:04:34,240
to still achieve their goals

133
00:04:32,720 --> 00:04:36,160
but you know there's other things like

134
00:04:34,240 --> 00:04:37,280
tools that exist to help us find

135
00:04:36,160 --> 00:04:39,520
vulnerabilities whether you're a

136
00:04:37,280 --> 00:04:41,199
defender or whether you're a hunter you

137
00:04:39,520 --> 00:04:43,600
know these tools can help you find

138
00:04:41,199 --> 00:04:45,280
things faster whether it's fuzzing with

139
00:04:43,600 --> 00:04:47,040
or without source code you know an

140
00:04:45,280 --> 00:04:48,720
attacker doesn't need source code to

141
00:04:47,040 --> 00:04:50,800
actually fuzz the code they can just

142
00:04:48,720 --> 00:04:52,800
feed a bunch of random binary data into

143
00:04:50,800 --> 00:04:54,880
the thing and consequently find

144
00:04:52,800 --> 00:04:57,919
vulnerabilities that way but also things

145
00:04:54,880 --> 00:04:59,360
like sanitizers give a defender an edge

146
00:04:57,919 --> 00:05:01,440
if they've got the source code and the

147
00:04:59,360 --> 00:05:03,759
attackers don't but for open source

148
00:05:01,440 --> 00:05:06,320
projects then the attackers can just go

149
00:05:03,759 --> 00:05:08,080
ahead and compile something with asan

150
00:05:06,320 --> 00:05:10,320
run it in a fuzzer and have the

151
00:05:08,080 --> 00:05:11,840
vulnerabilities dump out very easily and

152
00:05:10,320 --> 00:05:13,919
of course there's also exploit

153
00:05:11,840 --> 00:05:15,919
mitigations and you know some companies

154
00:05:13,919 --> 00:05:18,639
invest in sort of mitigation side of the

155
00:05:15,919 --> 00:05:20,880
world extremely deeply and this has been

156
00:05:18,639 --> 00:05:22,960
shown to make exploitation much much

157
00:05:20,880 --> 00:05:25,199
more difficult on the flip side of

158
00:05:22,960 --> 00:05:27,039
things for those places that don't

159
00:05:25,199 --> 00:05:29,039
invest in mitigations you know if you're

160
00:05:27,039 --> 00:05:31,759
a firmware developer or you're you know

161
00:05:29,039 --> 00:05:33,600
some custom hypervisor maker or you know

162
00:05:31,759 --> 00:05:35,199
certain open source projects if you

163
00:05:33,600 --> 00:05:37,360
don't invest in mitigations at the end

164
00:05:35,199 --> 00:05:40,080
of the day you're just making it cheap

165
00:05:37,360 --> 00:05:42,240
to attack you so as always let's all

166
00:05:40,080 --> 00:05:44,639
remember to program paranoid because

167
00:05:42,240 --> 00:05:48,560
it's not paranoia if they really are out

168
00:05:44,639 --> 00:05:48,560
to get you until next time


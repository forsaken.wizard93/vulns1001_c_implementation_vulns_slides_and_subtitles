1
00:00:00,160 --> 00:00:02,960
heap buffer overflows

2
00:00:01,920 --> 00:00:04,799
what is it

3
00:00:02,960 --> 00:00:07,120
well just like stack buffer overflow

4
00:00:04,799 --> 00:00:09,120
it's when too much ACID data gets copied

5
00:00:07,120 --> 00:00:11,040
into a buffer it's just on the heap this

6
00:00:09,120 --> 00:00:12,480
time instead of the stack

7
00:00:11,040 --> 00:00:13,840
so let's again introduce some

8
00:00:12,480 --> 00:00:15,519
conventions we're going to start from

9
00:00:13,840 --> 00:00:17,279
the same conventions as the stack

10
00:00:15,519 --> 00:00:18,960
diagrams for the same colors for

11
00:00:17,279 --> 00:00:20,640
uninitialized initialized attack

12
00:00:18,960 --> 00:00:23,199
controlled and semi-attacker-controlled

13
00:00:20,640 --> 00:00:25,359
data but now in the context of heaps we

14
00:00:23,199 --> 00:00:27,519
need to add this yellow color for data

15
00:00:25,359 --> 00:00:29,679
that the attacker potentially causes to

16
00:00:27,519 --> 00:00:32,239
be allocated but which they may or may

17
00:00:29,679 --> 00:00:34,000
not actually control the contents this

18
00:00:32,239 --> 00:00:36,079
is data that's going to be used for

19
00:00:34,000 --> 00:00:38,879
manipulating and massaging the layout of

20
00:00:36,079 --> 00:00:42,320
the heap to increase exploitability

21
00:00:38,879 --> 00:00:44,239
now we have our just 64 byte box

22
00:00:42,320 --> 00:00:46,160
instead of low addresses low and high

23
00:00:44,239 --> 00:00:48,320
addresses high we are going to flip it

24
00:00:46,160 --> 00:00:50,480
upside down and have low addresses high

25
00:00:48,320 --> 00:00:52,160
and high address is low this is a little

26
00:00:50,480 --> 00:00:54,800
bit more like what you would see if you

27
00:00:52,160 --> 00:00:56,879
were looking at something in a debugger

28
00:00:54,800 --> 00:00:59,039
then we are going to have the big end or

29
00:00:56,879 --> 00:01:01,440
most significant bytes to the right and

30
00:00:59,039 --> 00:01:04,159
little end to the left so up is down

31
00:01:01,440 --> 00:01:06,720
left is right and short is long and as

32
00:01:04,159 --> 00:01:08,720
usual memory rights happen from low to

33
00:01:06,720 --> 00:01:11,040
high addresses so on this diagram it's

34
00:01:08,720 --> 00:01:13,680
going to be from top to bottom

35
00:01:11,040 --> 00:01:16,240
and if we had a array on the heap we

36
00:01:13,680 --> 00:01:20,080
would just draw it as this so index 0 1

37
00:01:16,240 --> 00:01:22,000
2 3 etc all the way up to index 63. if

38
00:01:20,080 --> 00:01:24,560
we had an 8 byte value displayed on the

39
00:01:22,000 --> 00:01:27,600
heap like this in bytes then when we

40
00:01:24,560 --> 00:01:29,280
convert it to an eight byte value it

41
00:01:27,600 --> 00:01:31,360
would be flipped around because we said

42
00:01:29,280 --> 00:01:32,960
the most significant byte is on the

43
00:01:31,360 --> 00:01:35,439
right side and the least significant on

44
00:01:32,960 --> 00:01:36,320
the left so it's one one two two three

45
00:01:35,439 --> 00:01:38,960
three

46
00:01:36,320 --> 00:01:40,880
flipped around so it's eighty seven

47
00:01:38,960 --> 00:01:42,960
seven and so forth

48
00:01:40,880 --> 00:01:44,560
all right then these strings look a

49
00:01:42,960 --> 00:01:46,399
little bit more a little better than

50
00:01:44,560 --> 00:01:49,360
they did in the stack diagram so it's a

51
00:01:46,399 --> 00:01:52,399
little more readable slash home slash

52
00:01:49,360 --> 00:01:54,240
user etc and so that's it for our heap

53
00:01:52,399 --> 00:01:56,640
diagrams so once again what is a heat

54
00:01:54,240 --> 00:01:59,040
buffer overflow it's too much ACID data

55
00:01:56,640 --> 00:02:01,119
copied into a buffer on the heap

56
00:01:59,040 --> 00:02:03,119
so if we go back to that typical buffer

57
00:02:01,119 --> 00:02:05,280
overflow let's run through that again we

58
00:02:03,119 --> 00:02:07,680
got a memcpy and it's copying from the

59
00:02:05,280 --> 00:02:10,000
ACID buff to the vulnerable buff our

60
00:02:07,680 --> 00:02:11,840
diagram has been flipped upside down

61
00:02:10,000 --> 00:02:14,800
ACID buff is here vulnerable buff is

62
00:02:11,840 --> 00:02:17,760
there so the copy starts out and it

63
00:02:14,800 --> 00:02:19,280
copies from left to right and ultimately

64
00:02:17,760 --> 00:02:23,200
is going to overflow the vulnerable

65
00:02:19,280 --> 00:02:25,440
buffer and corrupt some target data

66
00:02:23,200 --> 00:02:27,280
so the root causes of heap overflows are

67
00:02:25,440 --> 00:02:28,879
exactly the same as the root causes of

68
00:02:27,280 --> 00:02:30,800
stack overflows it's the return of the

69
00:02:28,879 --> 00:02:33,280
sweet potato and carrot it's going to be

70
00:02:30,800 --> 00:02:35,440
situations where you have unbounded or

71
00:02:33,280 --> 00:02:37,920
weakly bounded functions that do you

72
00:02:35,440 --> 00:02:41,120
know memory or string operations and

73
00:02:37,920 --> 00:02:43,280
those ACID controlled exit conditions on

74
00:02:41,120 --> 00:02:45,040
loops that are doing memory copies

75
00:02:43,280 --> 00:02:47,040
here's our trivial example from the

76
00:02:45,040 --> 00:02:49,040
stack converted to a heap buffer

77
00:02:47,040 --> 00:02:51,040
overflow instead what's the difference

78
00:02:49,040 --> 00:02:53,519
instead of the buff on the stack as an

79
00:02:51,040 --> 00:02:55,920
array as a local variable it is now

80
00:02:53,519 --> 00:02:58,480
malloc 8 in order to dynamically

81
00:02:55,920 --> 00:03:00,560
allocate from the heap and 8 byte buffer

82
00:02:58,480 --> 00:03:03,519
then once again we do strcpy from

83
00:03:00,560 --> 00:03:05,040
arc v1 into the buffer and if arc v1 is

84
00:03:03,519 --> 00:03:06,400
too big that's going to cause a heap

85
00:03:05,040 --> 00:03:08,400
overflow

86
00:03:06,400 --> 00:03:11,120
walking through it once more we have the

87
00:03:08,400 --> 00:03:13,120
semi-attacker controlled value in rv 0

88
00:03:11,120 --> 00:03:15,200
which is the location where

89
00:03:13,120 --> 00:03:17,360
the program is actually executed from

90
00:03:15,200 --> 00:03:19,040
again semi-attacker controlled because

91
00:03:17,360 --> 00:03:21,200
can't necessarily have it go wherever

92
00:03:19,040 --> 00:03:23,760
they want then we've got that string

93
00:03:21,200 --> 00:03:26,000
that's going to be arg v of 1 and again

94
00:03:23,760 --> 00:03:29,360
semi-attack are controlled only in so

95
00:03:26,000 --> 00:03:32,239
far as a strcpy can't copy all

96
00:03:29,360 --> 00:03:33,920
possible byte values it only can copies

97
00:03:32,239 --> 00:03:35,680
what it considers valid

98
00:03:33,920 --> 00:03:38,239
ascii string values

99
00:03:35,680 --> 00:03:40,720
so okay now before main is actually

100
00:03:38,239 --> 00:03:43,120
executed maybe some other values in here

101
00:03:40,720 --> 00:03:45,120
will be initialized between the

102
00:03:43,120 --> 00:03:47,440
locations here these are going to be on

103
00:03:45,120 --> 00:03:49,200
the stack and then you know the stack

104
00:03:47,440 --> 00:03:51,440
and the heap are you know far away from

105
00:03:49,200 --> 00:03:53,680
each other so probably some data in

106
00:03:51,440 --> 00:03:56,080
there is going to be initialized

107
00:03:53,680 --> 00:03:57,519
then main is called and then there's

108
00:03:56,080 --> 00:04:00,319
going to be some you know initialized or

109
00:03:57,519 --> 00:04:03,439
uninitialized data we don't know and

110
00:04:00,319 --> 00:04:05,200
buff is allocated on the heap so again

111
00:04:03,439 --> 00:04:06,560
this is going to be the stack this is

112
00:04:05,200 --> 00:04:08,400
going to be some heap data there's a

113
00:04:06,560 --> 00:04:10,319
whole bunch of space in between them

114
00:04:08,400 --> 00:04:13,360
represented by a single line of dot dot

115
00:04:10,319 --> 00:04:15,599
dots so eight bytes on the heap it is

116
00:04:13,360 --> 00:04:17,680
now shown in gray to indicate that it is

117
00:04:15,599 --> 00:04:19,199
uninitialized data until someone writes

118
00:04:17,680 --> 00:04:20,959
something in there

119
00:04:19,199 --> 00:04:24,479
and then when the strcpy happy

120
00:04:20,959 --> 00:04:26,880
happens arcv1 will be copied to buff and

121
00:04:24,479 --> 00:04:29,360
it will ultimately overflow buff on the

122
00:04:26,880 --> 00:04:31,520
heap and corrupt whatever data is

123
00:04:29,360 --> 00:04:32,960
adjacent to it so we don't know if it

124
00:04:31,520 --> 00:04:35,440
was initialized data we don't know if it

125
00:04:32,960 --> 00:04:37,199
was uninitialized data you know what got

126
00:04:35,440 --> 00:04:39,120
smashed what got overwritten how does

127
00:04:37,199 --> 00:04:41,040
the attacker take control based on this

128
00:04:39,120 --> 00:04:43,759
and why is it valuable to them

129
00:04:41,040 --> 00:04:45,280
well the answer is it depends so it

130
00:04:43,759 --> 00:04:47,199
depends on a lot of different things you

131
00:04:45,280 --> 00:04:49,520
know how far did the override occur was

132
00:04:47,199 --> 00:04:52,240
it a single byte overflow was it

133
00:04:49,520 --> 00:04:53,600
thousands of bytes overflow how full was

134
00:04:52,240 --> 00:04:55,280
the heap you know was the heat being

135
00:04:53,600 --> 00:04:57,520
completely unused thus far in this

136
00:04:55,280 --> 00:04:59,280
particular program or you know is this

137
00:04:57,520 --> 00:05:00,240
program part of you know some daemon

138
00:04:59,280 --> 00:05:01,680
that's got a whole bunch of other

139
00:05:00,240 --> 00:05:03,680
background activity and the heap

140
00:05:01,680 --> 00:05:04,960
overflow just happened at a certain time

141
00:05:03,680 --> 00:05:06,960
it's also going to matter how the

142
00:05:04,960 --> 00:05:08,800
allocator itself was designed there are

143
00:05:06,960 --> 00:05:12,000
different designs where for instance the

144
00:05:08,800 --> 00:05:14,240
metadata that handles linked lists used

145
00:05:12,000 --> 00:05:16,560
to organize heaps can be in line or they

146
00:05:14,240 --> 00:05:18,560
can be out of ban and also there's

147
00:05:16,560 --> 00:05:19,840
questions of was the heap allocator

148
00:05:18,560 --> 00:05:21,759
specifically

149
00:05:19,840 --> 00:05:24,160
written in a way that tries to defend

150
00:05:21,759 --> 00:05:27,280
against heap overflow exploits so all of

151
00:05:24,160 --> 00:05:29,680
that just is to say that it becomes an

152
00:05:27,280 --> 00:05:31,840
objective for an attacker to manipulate

153
00:05:29,680 --> 00:05:33,520
and control the heap in a way that

154
00:05:31,840 --> 00:05:35,120
benefits them so that they can know

155
00:05:33,520 --> 00:05:37,120
something about what they're overriding

156
00:05:35,120 --> 00:05:39,120
at the time that heap overflow occurs

157
00:05:37,120 --> 00:05:40,000
one of the most naive ways that they

158
00:05:39,120 --> 00:05:42,160
could do

159
00:05:40,000 --> 00:05:43,919
is just taking and doing what's called a

160
00:05:42,160 --> 00:05:45,759
heap spray where they just

161
00:05:43,919 --> 00:05:48,400
allocate a whole bunch of data that they

162
00:05:45,759 --> 00:05:49,199
control onto the stack and then you know

163
00:05:48,400 --> 00:05:51,199
they

164
00:05:49,199 --> 00:05:52,960
have no matter where in a particular

165
00:05:51,199 --> 00:05:54,479
range they're going to land if they're

166
00:05:52,960 --> 00:05:55,840
for instance trying to jump into code or

167
00:05:54,479 --> 00:05:58,080
something like that they could

168
00:05:55,840 --> 00:05:59,680
potentially guarantee that all of this

169
00:05:58,080 --> 00:06:01,360
heap range is going to be filled in with

170
00:05:59,680 --> 00:06:03,520
attacker-controlled data

171
00:06:01,360 --> 00:06:05,919
now that like I said is a sort of naive

172
00:06:03,520 --> 00:06:08,479
approach and it generally mostly worked

173
00:06:05,919 --> 00:06:10,639
back on 32-bit systems where if you

174
00:06:08,479 --> 00:06:12,240
allocated a ton of heap space perhaps in

175
00:06:10,639 --> 00:06:14,400
the context of a browser that's being

176
00:06:12,240 --> 00:06:16,880
exploited then you could like hard code

177
00:06:14,400 --> 00:06:19,440
an address of you know zero C zeros v

178
00:06:16,880 --> 00:06:21,600
zero C etc and you could pretty much

179
00:06:19,440 --> 00:06:23,520
guarantee that that would land somewhere

180
00:06:21,600 --> 00:06:25,520
in the heap that you had sprayed

181
00:06:23,520 --> 00:06:27,600
but generally speaking on 64-bit systems

182
00:06:25,520 --> 00:06:29,680
that doesn't tend to work instead what

183
00:06:27,600 --> 00:06:31,680
you see is what's called heap grooming

184
00:06:29,680 --> 00:06:34,479
or heap feng shui

185
00:06:31,680 --> 00:06:36,880
heap feng shui being a paper that's sort

186
00:06:34,479 --> 00:06:39,120
of coined to this term and nicely

187
00:06:36,880 --> 00:06:42,080
describes how one you know aligns the

188
00:06:39,120 --> 00:06:45,280
heap and you know modifies it to

189
00:06:42,080 --> 00:06:47,520
optimize the qi of the exploitation so

190
00:06:45,280 --> 00:06:50,000
now we introduce that yellow coloration

191
00:06:47,520 --> 00:06:51,919
in order to say that the attacker caused

192
00:06:50,000 --> 00:06:54,000
a bunch of allocations to occur and they

193
00:06:51,919 --> 00:06:56,479
may or may not actually control the

194
00:06:54,000 --> 00:06:58,960
values of these allocations

195
00:06:56,479 --> 00:07:01,199
then the attacker is going to do a

196
00:06:58,960 --> 00:07:02,800
selective deallocation of some

197
00:07:01,199 --> 00:07:05,440
particular memory area

198
00:07:02,800 --> 00:07:07,840
and that's going to lead to a hole in

199
00:07:05,440 --> 00:07:09,599
the memory map so a hole in the in the

200
00:07:07,840 --> 00:07:12,080
heap layout

201
00:07:09,599 --> 00:07:14,160
so now the attacker can cause some

202
00:07:12,080 --> 00:07:16,479
victim data that will be beneficial for

203
00:07:14,160 --> 00:07:19,199
them to overwrite later and cause that

204
00:07:16,479 --> 00:07:21,199
allocation to land in the hole basically

205
00:07:19,199 --> 00:07:23,440
you know upon analysis of a heap

206
00:07:21,199 --> 00:07:26,240
allocator you can you know make certain

207
00:07:23,440 --> 00:07:28,960
assumptions or guarantees about you know

208
00:07:26,240 --> 00:07:30,800
when and where the heap is going to

209
00:07:28,960 --> 00:07:32,639
allocate things based on what's

210
00:07:30,800 --> 00:07:34,240
available on the free list

211
00:07:32,639 --> 00:07:36,639
next the attacker will cause a

212
00:07:34,240 --> 00:07:38,800
deallocation to occur adjacent to the

213
00:07:36,639 --> 00:07:41,120
data that they want to overwrite and

214
00:07:38,800 --> 00:07:41,919
into that they cause the allocation of

215
00:07:41,120 --> 00:07:44,800
some

216
00:07:41,919 --> 00:07:46,800
buffer which they know can be overflowed

217
00:07:44,800 --> 00:07:48,400
with a heap buffer overflow

218
00:07:46,800 --> 00:07:50,960
vulnerability

219
00:07:48,400 --> 00:07:53,039
then ultimately they target that and

220
00:07:50,960 --> 00:07:55,520
force the override of that data

221
00:07:53,039 --> 00:07:57,280
ultimately overflowing into the victim

222
00:07:55,520 --> 00:07:59,919
data so the victim data is what they

223
00:07:57,280 --> 00:08:01,280
actually care about the target data is

224
00:07:59,919 --> 00:08:03,840
you know where they can start the

225
00:08:01,280 --> 00:08:05,280
overflow from in order to overflow into

226
00:08:03,840 --> 00:08:08,400
the adjacent data

227
00:08:05,280 --> 00:08:10,160
now a great victim type of data is

228
00:08:08,400 --> 00:08:12,000
anything that includes something like

229
00:08:10,160 --> 00:08:14,000
control flow data such as function

230
00:08:12,000 --> 00:08:17,199
pointers so if the attacker can cause

231
00:08:14,000 --> 00:08:19,520
some struct or some C plus object to be

232
00:08:17,199 --> 00:08:21,360
allocated into the space and if that

233
00:08:19,520 --> 00:08:23,440
includes function pointers then the

234
00:08:21,360 --> 00:08:25,120
overriding of this will override a

235
00:08:23,440 --> 00:08:27,440
function pointer which eventually the

236
00:08:25,120 --> 00:08:29,520
attacker knows will get called and that

237
00:08:27,440 --> 00:08:32,159
will lead to jumping to an arbitrary

238
00:08:29,520 --> 00:08:33,839
location that the attacker controls

239
00:08:32,159 --> 00:08:35,440
now in practice it's more complicated

240
00:08:33,839 --> 00:08:36,880
than this as you'll see when we get to

241
00:08:35,440 --> 00:08:39,120
one of the examples that walks you

242
00:08:36,880 --> 00:08:40,640
through a full exploit but you know

243
00:08:39,120 --> 00:08:42,719
conceptually for instance because the

244
00:08:40,640 --> 00:08:44,800
attacker can't necessarily control

245
00:08:42,719 --> 00:08:46,080
exactly exactly where something happens

246
00:08:44,800 --> 00:08:47,519
well they just do the same thing

247
00:08:46,080 --> 00:08:48,720
multiple times and they don't

248
00:08:47,519 --> 00:08:50,880
necessarily know which of these

249
00:08:48,720 --> 00:08:52,800
vulnerable buffers is going to overflow

250
00:08:50,880 --> 00:08:54,880
but it works all the same as long as

251
00:08:52,800 --> 00:08:56,880
they corrupt the victim data of interest

252
00:08:54,880 --> 00:08:59,440
so all right let's go look at some real

253
00:08:56,880 --> 00:08:59,440
examples


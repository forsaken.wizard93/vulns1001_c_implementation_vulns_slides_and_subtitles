1
00:00:00,640 --> 00:00:05,520
what can you do to find vulnerabilities

2
00:00:02,960 --> 00:00:07,759
in code whether yours or someone else's

3
00:00:05,520 --> 00:00:10,240
well you can read the fun code

4
00:00:07,759 --> 00:00:11,920
yes the heuristics and GUIDance that we

5
00:00:10,240 --> 00:00:13,599
provide in this class are mostly focused

6
00:00:11,920 --> 00:00:15,599
around the things like following the

7
00:00:13,599 --> 00:00:17,520
ACID flow through the code

8
00:00:15,599 --> 00:00:21,039
and if you look for these common root

9
00:00:17,520 --> 00:00:23,519
causes like unsafe functions or

10
00:00:21,039 --> 00:00:25,760
attacker controlled memory copies

11
00:00:23,519 --> 00:00:28,000
occurring in loops you will almost

12
00:00:25,760 --> 00:00:30,000
certainly find vulnerabilities really

13
00:00:28,000 --> 00:00:31,679
all comes down to whether the people

14
00:00:30,000 --> 00:00:34,640
have been programming paranoid and

15
00:00:31,679 --> 00:00:36,719
adding appropriate sanity checks or not

16
00:00:34,640 --> 00:00:38,879
now it's often the case that defenders

17
00:00:36,719 --> 00:00:40,960
will have a first mover advantage in the

18
00:00:38,879 --> 00:00:43,120
sense that if you're not currently under

19
00:00:40,960 --> 00:00:45,200
attack if you go in there today and

20
00:00:43,120 --> 00:00:46,960
start adding sanity checks and start you

21
00:00:45,200 --> 00:00:49,039
know evaluating where the attacker

22
00:00:46,960 --> 00:00:50,879
controlled input comes into your code

23
00:00:49,039 --> 00:00:52,480
you can go ahead and close down a lot of

24
00:00:50,879 --> 00:00:53,520
avenues before the attacker actually

25
00:00:52,480 --> 00:00:55,680
gets there

26
00:00:53,520 --> 00:00:58,000
but if the attacker is already there as

27
00:00:55,680 --> 00:01:00,399
they often are they can also have the

28
00:00:58,000 --> 00:01:02,399
advantage of sometimes the defender may

29
00:01:00,399 --> 00:01:04,960
not think something is ACID but it

30
00:01:02,399 --> 00:01:07,360
actually is or the attacker can have the

31
00:01:04,960 --> 00:01:09,280
advantage of time of being able to focus

32
00:01:07,360 --> 00:01:11,360
on one particular component for a really

33
00:01:09,280 --> 00:01:13,439
long time as opposed to the defender

34
00:01:11,360 --> 00:01:15,439
having to you know allocate their time

35
00:01:13,439 --> 00:01:17,600
amongst the various inputs so if an

36
00:01:15,439 --> 00:01:19,439
attacker really really wants to get in

37
00:01:17,600 --> 00:01:21,759
from a particular thing they're going to

38
00:01:19,439 --> 00:01:24,080
spend a lot of time on it think you know

39
00:01:21,759 --> 00:01:26,080
if they want to find a vulnerability in

40
00:01:24,080 --> 00:01:29,280
some messaging application that they

41
00:01:26,080 --> 00:01:31,680
know a victim uses or a web browser or

42
00:01:29,280 --> 00:01:34,479
you know some custom control software

43
00:01:31,680 --> 00:01:36,159
that you know controls your plc's on

44
00:01:34,479 --> 00:01:37,360
your nuclear enrichment program

45
00:01:36,159 --> 00:01:38,720
something like that

46
00:01:37,360 --> 00:01:40,240
right so

47
00:01:38,720 --> 00:01:42,320
it's often the case that over time the

48
00:01:40,240 --> 00:01:44,159
defender's advantage goes away as the

49
00:01:42,320 --> 00:01:46,159
attacker learns everything more

50
00:01:44,159 --> 00:01:48,000
thoroughly so if you're a defender you

51
00:01:46,159 --> 00:01:50,000
know it really behooves you to start

52
00:01:48,000 --> 00:01:52,399
right away and follow that ACID flow

53
00:01:50,000 --> 00:01:52,399
today

